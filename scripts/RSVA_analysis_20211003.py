with open("primers/RSVA_primers_complemented_20210930.fasta") as file1, \
     open("raw_sequences/RSVA_WGS_with_primers_aligned_edited_20210930.fasta") as file2, \
     open("RSVA_primer_checks_20211003.txt","w") as outfile1:
    primer_dictionary = {}
    primer_start_sites = {}
    primer_list = []
    list_nucleotides = ["a","c","g","t"]
    outfile1.write("sequence_id\tprimer\tnumber_mismatches\tprimer_sequence\tviral_sequence\n")
    for line in file1:
        if line.startswith(">"):
            identifier = line.rstrip()
            identifier = identifier[1:]
            sequence_id = identifier.split("-")[0]
            sequence_start_point = int(identifier.split("-")[1])
            primer_list.append(sequence_id)
            primer_start_sites[sequence_id] =sequence_start_point
        else:
            sequence = line.rstrip()
            sequence = sequence.lower()
            primer_dictionary[sequence_id]= sequence
    for line in file2:
        if line.startswith(">"):
            identifier = line.rstrip()
        else:
            sequence = line.rstrip()
            for primer in primer_list:
                count = 0
                primer_start_site = primer_start_sites[primer]
                primer_sequence = primer_dictionary[primer]
                end_site = primer_start_site + len(primer_sequence)
                sequence_to_check = sequence[primer_start_site:end_site]
                for i in range(0,len(primer_sequence)):
                    if primer_sequence[i] in list_nucleotides:
                        if primer_sequence[i] != sequence_to_check[i]:
                            count += 1
                    else:
                        if primer_sequence == "y":
                            if sequence_to_check == "a" or sequence_check != "g":
                                count +=1
                        elif primer_sequence == "r":
                            if sequence_to_check == "t" or sequence_check != "c":
                                count += 1
                if count > 0:
                    outfile1.write(identifier + "\t" + primer + "\t"+ str(count)+ "\t" + primer_sequence + "\t" + sequence_to_check + "\n")
                
                    
                    
         
        
